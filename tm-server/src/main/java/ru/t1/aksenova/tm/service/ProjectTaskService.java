package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.entity.TaskNotFoundException;
import ru.t1.aksenova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aksenova.tm.exception.field.TaskIdEmptyException;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    public final IProjectRepository projectRepository;

    @NotNull
    public final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeOneById(userId, task.getId());
        projectRepository.removeOneById(userId, projectId);
    }

}
