package ru.t1.aksenova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.component.ISaltProvider;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull ISaltProvider salt);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull ISaltProvider salt);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role, @NotNull ISaltProvider salt);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
