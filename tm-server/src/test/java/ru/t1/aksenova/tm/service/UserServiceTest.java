package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.repository.IUserRepository;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.IUserService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.TaskRepository;
import ru.t1.aksenova.tm.repository.UserRepository;

import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IUserService service = new UserService(propertyService, repository, taskRepository, projectRepository);

    @Before
    public void before() {
        repository.add(ADMIN_TEST);
    }

    @After
    public void after() {
        repository.removeAll();
    }

    @Test
    public void create() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, USER_TEST_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> service.create("", USER_TEST_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getLogin(), USER_TEST_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, null));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, ""));
        @NotNull final User user = service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        Assert.assertEquals(user, service.findByLogin(USER_TEST_LOGIN));
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, USER_TEST_PASSWORD, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> service.create("", USER_TEST_PASSWORD, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getLogin(), USER_TEST_PASSWORD, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, null, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, "", USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, ADMIN_TEST.getEmail()));
        @NotNull final User user = service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);
        Assert.assertEquals(user, service.findByLogin(USER_TEST_LOGIN));
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(USER_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(AbstractException.class, () -> service.create(null, USER_TEST_PASSWORD, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> service.create("", USER_TEST_PASSWORD, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> service.create(ADMIN_TEST.getLogin(), USER_TEST_PASSWORD, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, null, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, "", Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, (Role) null));
        @NotNull final User user = service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, Role.USUAL);
        Assert.assertEquals(user, service.findByLogin(USER_TEST_LOGIN));
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(USER_TEST.getRole(), user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(""));
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(NON_EXISTING_LOGIN));
        @Nullable final User user = service.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(AbstractException.class, () -> service.findByEmail(null));
        Assert.assertThrows(AbstractException.class, () -> service.findByEmail(""));
        Assert.assertThrows(AbstractException.class, () -> service.findByEmail(NON_EXISTING_EMAIL));
        @Nullable final User user = service.findByEmail(ADMIN_TEST.getEmail());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void isLoginExist() {
        Assert.assertFalse(repository.isLoginExist(""));
        Assert.assertFalse(repository.isLoginExist(NON_EXISTING_LOGIN));
        assert USER_TEST.getLogin() != null;
        Assert.assertTrue(repository.isLoginExist(ADMIN_TEST.getLogin()));
    }

    @Test
    public void isEmailExist() {
        Assert.assertFalse(repository.isEmailExist(""));
        Assert.assertFalse(repository.isEmailExist(NON_EXISTING_EMAIL));
        assert ADMIN_TEST.getEmail() != null;
        Assert.assertTrue(repository.isEmailExist(ADMIN_TEST.getEmail()));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(""));
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(NON_EXISTING_EMAIL));
        @Nullable final User user = service.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user);
        service.lockUserByLogin(user.getLogin());
        Assert.assertTrue(user.isLocked());

    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin(""));
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin(NON_EXISTING_EMAIL));
        @Nullable final User user = service.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user);
        service.unlockUserByLogin(user.getLogin());
        Assert.assertFalse(user.isLocked());
    }

    @Test
    public void removeOne() {
        Assert.assertNull(service.removeOne(null));
        @Nullable final User user2 = service.add(USER_TEST);
        Assert.assertNotNull(service.findOneById(USER_TEST.getId()));
        service.removeOne(service.removeOne(user2));
        Assert.assertNull(service.findOneById(USER_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneById(""));
        Assert.assertNull(service.removeOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = service.add(USER_TEST);
        Assert.assertNotNull(service.findOneById(USER_TEST.getId()));
        service.removeOneById(user.getId());
        Assert.assertNull(service.findOneById(USER_TEST.getId()));
    }

    @Test
    public void removeOneByEmail() {
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByEmail(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByEmail(""));
        Assert.assertThrows(AbstractException.class, () -> service.removeOneByEmail(NON_EXISTING_EMAIL));
        @Nullable final User user = service.add(USER_TEST);
        Assert.assertNotNull(service.findOneById(USER_TEST.getId()));
        service.removeOneByEmail(user.getEmail());
        Assert.assertNull(service.findOneById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findOneById(""));
        Assert.assertNull(service.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = service.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST, user);
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(AbstractException.class, () -> service.setPassword(null, NON_EXISTING_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> service.setPassword("", NON_EXISTING_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> service.setPassword(ADMIN_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> service.setPassword(ADMIN_TEST.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> service.setPassword(NON_EXISTING_USER_ID, NON_EXISTING_PASSWORD));
        @Nullable final User user = service.findOneById(ADMIN_TEST.getId());
        service.setPassword(ADMIN_TEST.getId(), "admin2");
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(AbstractException.class, () -> service.updateUser(null, FIRST_NAME, LAST_NAME, MIDDLE_NAME));
        Assert.assertThrows(AbstractException.class, () -> service.updateUser("", FIRST_NAME, LAST_NAME, MIDDLE_NAME));
        Assert.assertThrows(AbstractException.class, () -> service.updateUser(NON_EXISTING_USER_ID, FIRST_NAME, LAST_NAME, MIDDLE_NAME));
        @Nullable final User user = service.findOneById(ADMIN_TEST.getId());
        service.updateUser(ADMIN_TEST.getId(), FIRST_NAME, LAST_NAME, MIDDLE_NAME);
        Assert.assertEquals(FIRST_NAME, ADMIN_TEST.getFirstName());
        Assert.assertEquals(LAST_NAME, ADMIN_TEST.getLastName());
        Assert.assertEquals(MIDDLE_NAME, ADMIN_TEST.getMiddleName());
    }

}
