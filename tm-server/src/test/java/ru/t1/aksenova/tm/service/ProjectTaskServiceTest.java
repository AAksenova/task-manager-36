package ru.t1.aksenova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.repository.ProjectRepository;
import ru.t1.aksenova.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);


    @Before
    public void before() {
        projectRepository.add(USER_PROJECT1);
        projectRepository.add(USER_PROJECT2);
        taskRepository.add(USER_TASK1);
        taskRepository.add(USER_TASK2);
        taskRepository.add(USER_TASK3);
    }

    @After
    public void after() {
        taskRepository.removeAll();
        projectRepository.removeAll();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ADMIN_TASK1.getId()));
        projectTaskService.bindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final Task task = taskRepository.findOneById(USER_TEST.getId(), USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), ADMIN_TASK1.getId()));
        projectTaskService.unbindTaskToProject(USER_TEST.getId(), USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final Task task = taskRepository.findOneById(USER_TEST.getId(), USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(null, USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(USER_TEST.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));

        @NotNull List<Task> tasks = taskRepository.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(tasks);
        projectTaskService.removeTaskToProject(USER_TEST.getId(), USER_PROJECT1.getId());
        tasks = taskRepository.findAllByProjectId(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

}
